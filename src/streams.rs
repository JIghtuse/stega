extern crate rand;

use rand::{Rng,StdRng,SeedableRng};

pub struct SequentialStream {
    current: uint,
    end: uint,
}

impl SequentialStream {
    pub fn new(n: uint) -> SequentialStream {
        SequentialStream { current: -1, end: n }
    }
}

impl Iterator<uint> for SequentialStream {
    fn next(&mut self) -> Option<uint> {
        if self.current == self.end {
            None
        } else {
            self.current += 1;
            Some(self.current)
        }
    }
}

pub struct RandomStream {
    current: uint,
    end: uint,
    array: Vec<uint>,
}

impl RandomStream {
    pub fn new(n: uint, seed: uint) -> RandomStream {
        let mut stream = RandomStream{ current: -1, end: n, array: vec!() };
        for i in range(0, stream.end) {
            stream.array.push(i);
        }
        let mut rng : StdRng = SeedableRng::from_seed(&[seed]);
        rng.shuffle(stream.array.as_mut_slice());
        stream
    }
}

impl Iterator<uint> for RandomStream {
    fn next(&mut self) -> Option<uint> {
        if self.current == self.end {
            None
        } else {
            self.current += 1;
            Some(*self.array.get(self.current))
        }
    }
}

