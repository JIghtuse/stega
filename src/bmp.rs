use std::io::File;
use std::iter::range_step;
use std::vec::Vec;

pub struct Pixel {
    pub r: u8,
    pub g: u8,
    pub b: u8
}

impl Clone for Pixel {
    fn clone(&self) -> Pixel {
        Pixel{ r: self.r, g: self.g, b: self.b }
    }
}

struct BitmapFileHeader {
    header: Vec<char>,
    fsize: u32,
    res1: u16,
    res2: u16,
    data_offset: uint
}

#[inline]

pub struct BitmapContainer {
    header: Vec<u8>,
    pub size: uint,
    pixels: Vec<Pixel>,
}

impl BitmapContainer {
    pub fn getpixel(&self, pos: uint) -> Pixel {
        return *self.pixels.get(pos);
    }
    pub fn putpixel(&mut self, pos: uint, p: Pixel) {
        self.pixels.grow_set(pos, &p, p);
    }
}

fn u8_pack_u32(arr: &[u8]) -> u32 {
    return arr[0] as u32
         | arr[1] as u32 << 8
         | arr[2] as u32 << 16
         | arr[3] as u32 << 24;
}
#[inline]
fn u8_pack_u16(arr: &[u8]) -> u16 {
    return arr[0] as u16 | arr[1] as u16 << 8;
}

fn get_npixels(contents: &Vec<u8>) -> uint {
    let dib_header_sz = u8_pack_u32(contents.slice(14, 18));
    let mut npixels : u32;
    if dib_header_sz == 12 {
        npixels = u8_pack_u16(contents.slice(18, 20)) as u32
                * u8_pack_u16(contents.slice(20, 22)) as u32;
    } else {
        npixels = u8_pack_u32(contents.slice(18, 22)) as u32
                * u8_pack_u32(contents.slice(22, 26)) as u32;
    }
    return npixels as uint;
}

pub fn load_image(fname: &str) -> BitmapContainer {
    let path = Path::new(fname);
    let mut file = match File::open(&path) {
        Ok(f) => f,
        Err(e) => fail!("file opening error: {}", e),
    };

    let mut contents = file.read_to_end().unwrap();
    let fheader = BitmapFileHeader {
        header: vec!(*contents.get(0) as char, *contents.get(1) as char),
        fsize: u8_pack_u32(contents.slice(2,6)),
        res1: u8_pack_u16(contents.slice(6,8)),
        res2: u8_pack_u16(contents.slice(8, 10)),
        data_offset: u8_pack_u32(contents.slice(10, 14)) as uint,
    };
    // FIXME: we need to check header and bpp here

    let npixels = get_npixels(&contents);

    let ncolors = 3;
    let mut pixels = vec!();
    for i in range_step(0, ncolors * npixels, ncolors) {
        let idx = fheader.data_offset + i;
        let pixel = Pixel{
            b : *contents.get(idx),
            g : *contents.get(idx + 1),
            r : *contents.get(idx + 2),
        };
        pixels.push(pixel);
    }
    // now only headers in contents
    contents.truncate(fheader.data_offset);
    return BitmapContainer{ header: contents, size: npixels, pixels: pixels };
}

pub fn save_image(fname: &str, container: BitmapContainer) -> bool {
    let mut file = File::create(&Path::new(fname));

    file.write(container.header.as_slice()).unwrap();

    for i in range(0, container.size) {
        file.write_u8(container.pixels.get(i).b).unwrap();
        file.write_u8(container.pixels.get(i).g).unwrap();
        file.write_u8(container.pixels.get(i).r).unwrap();
    }

    return true;
}
