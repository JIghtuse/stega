use std::iter::range_step;
use std::str;
use streams::{SequentialStream,RandomStream};
use bmp::{BitmapContainer,Pixel,save_image,load_image};
use string_gen::gen_str;

fn inject_bit(pixel: Pixel, bit: u8) -> Pixel {
    let new_b = match bit {
        0 => (pixel.b >> 1) << 1,
        1 => pixel.b | 1,
        _ => pixel.b,
    };

    return Pixel { 
        r: pixel.r,
        g: pixel.g,
        b: new_b,
    };
}

fn encrypt_rnd(mut container: BitmapContainer, data: &str, key: &str) -> BitmapContainer {
    assert!(8 * data.bytes().len() <= container.size);

    let mut position_iterator = RandomStream::new(8 * data.bytes().len(), 1);

    let bytes = data.as_bytes();
    let key_bytes = key.as_bytes();

    for j in range(0, bytes.len()) {
        for i in range_step(7, -1, -1) {
            let pos = position_iterator.next().unwrap();
            let init_pixel = container.getpixel(pos);

            let r = 1 & (key_bytes[j] >> i);
            let s = 1 & (bytes[j] >> i);
            let x = 1 & init_pixel.b;

            let mut bit = x;
            if x == s {
                bit = x;
            } else if r == 1 {
                // TODO: forbid +1 at 255
                bit = x + 1;
            } else if r == 0 {
                // TODO: forbid -1 at 0
                bit = x - 1;
            }
            let pixel = inject_bit(init_pixel, bit);

            container.putpixel(pos, pixel);
        }
    }
    return container;
}

fn encrypt_seq(mut container: BitmapContainer, data: &str, key: &str) -> BitmapContainer {
    assert!(8 * data.bytes().len() <= container.size);

    let mut position_iterator = SequentialStream::new(8 * data.bytes().len());

    let bytes = data.as_bytes();
    let key_bytes = key.as_bytes();

    for j in range(0, bytes.len()) {
        for i in range_step(7, -1, -1) {
            let pos = position_iterator.next().unwrap();
            let init_pixel = container.getpixel(pos);

            let r = 1 & (key_bytes[j] >> i);
            let s = 1 & (bytes[j] >> i);
            let x = 1 & init_pixel.b;

            let mut bit = x;
            if x == s {
                bit = x;
            } else if r == 1 {
                // TODO: forbid +1 at 255
                bit = x + 1;
            } else if r == 0 {
                // TODO: forbid -1 at 0
                bit = x - 1;
            }
            let pixel = inject_bit(init_pixel, bit);

            container.putpixel(pos, pixel);
        }
    }
    return container;
}

fn decrypt_rnd(container: &BitmapContainer, sz: uint, key: &str) -> ~str {
    assert!(8 * sz <= container.size);

    let mut position_iterator = RandomStream::new(8 * sz, 1);
    let mut res = vec!();

    let key_bytes = key.as_bytes();

    for i in range(0, sz) {
        let mut byte : u8 = 0;
        for j in range(0u, 8) {
            let pixel = container.getpixel(position_iterator.next().unwrap());
            let mut bit = (pixel.b) & 1;

            let r = 1 & (key_bytes[i] >> j);
            if r == 1 {
                bit -= 1;
            }
            // if (r == 0) {
            //     bit -= 1;
            // } else {
            //     bit += 1;
            // }
            // if x == s {
            //     bit = x;
            // } else if r == 1 {
            //     // TODO: forbid +1 at 255
            //     bit = x + 1;
            // } else if r == 0 {
            //     // TODO: forbid -1 at 0
            //     bit = x - 1;


            byte <<= 1;
            byte |= bit;
        }
        res.push(byte);
    }
    let v : ~[u8] = res.as_slice().to_owned();
    return str::from_utf8(v).unwrap().to_owned();
}

fn decrypt_seq(container: &BitmapContainer, sz: uint, key: &str) -> ~str {
    assert!(8 * sz <= container.size);

    let mut position_iterator = SequentialStream::new(8 * sz);
    let mut res = vec!();

    let key_bytes = key.as_bytes();

    for i in range(0u, sz) {
        let mut byte : u8 = 0;
        for j in range(0u, 8) {
            let pixel = container.getpixel(position_iterator.next().unwrap());
            let mut bit = (pixel.b) & 1;

            let r = 1 & (key_bytes[i] >> j);
            if r == 1 {
                bit -= 1;
            }
            // if (r == 0) {
            //     bit -= 1;
            // } else {
            //     bit += 1;
            // }
            byte <<= 1;
            byte |= bit;
        }
        res.push(byte);
    }
    let v : ~[u8] = res.as_slice().to_owned();
    return str::from_utf8(v).unwrap().to_owned();
}

pub fn inject_seq(fname: &str, percentage: uint) -> f32 {
    let mut container = load_image(fname);

    let s = gen_str(container.size, percentage);
    let key = gen_str(container.size, percentage);
    let actual_inject = s.len() as f32 / (container.size as f32 / 8.0) * 100.0;

    container = encrypt_seq(container, s, key);
    let res = decrypt_seq(&container, s.bytes().len(), key);
    println!("was: '{}' becomes: '{}'", s, res);
    assert!(s == res);
    save_image(fname + "_rseq", container);

    return actual_inject;
}

pub fn inject_rnd(fname: &str, percentage: uint) -> f32 {
    let mut container = load_image(fname);

    let s = gen_str(container.size, percentage);
    let key = gen_str(container.size, percentage);
    let actual_inject = s.len() as f32 / (container.size as f32 / 8.0) * 100.0;

    container = encrypt_rnd(container, s, key);
    let res = decrypt_rnd(&container, s.bytes().len(), key);
    println!("was: '{}' becomes: '{}'", s, res);
    assert!(s == res);
    save_image(fname + "_rrand", container);

    return actual_inject;
}
