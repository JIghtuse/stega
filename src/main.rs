extern crate rand;

mod streams;
mod bmp;
mod lsb_replacement;
mod string_gen;
mod lsb_matching;

fn inject_data(method: ~str, fname: ~str, percentage: uint) -> f32 {
    let mut injected = 0.0;
    if method == "rseq".to_owned() {
        println!("Using sequential LSB replacement");
        injected = lsb_replacement::inject_seq(fname, percentage);
    } else if method == "rrnd".to_owned() {
        println!("Using random LSB replacement");
        injected = lsb_replacement::inject_rnd(fname, percentage);
    } else if method == "mseq".to_owned() {
        println!("Using sequential LSB matching");
        injected = lsb_matching::inject_seq(fname, percentage);
    } else if method == "mrnd".to_owned() {
        println!("Using random LSB matching");
        injected = lsb_matching::inject_rnd(fname, percentage);
    }
    return injected;
}

fn main() {
    let args = std::os::args();
    if args.len() < 4 {
        fail!("Usage: {:s} <method> <filename.bmp> <% data to include>", args[0]);
    }

    let percentage : uint = from_str(args[3]).unwrap();
    if percentage > 100 {
        fail!("Cannot inject more than 100% of data");
    }

    let injected = inject_data(args[1].clone(), args[2], percentage);
    println!("Injected {}% of data", injected);
}

