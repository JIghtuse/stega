use rand::{task_rng,Rng};

pub fn gen_str(container_sz: uint, percentage: uint) -> ~str {
    let strlen_to_inject = (container_sz / 8) * percentage / 100;
    return task_rng().gen_ascii_str(strlen_to_inject);
}
