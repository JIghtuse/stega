RUSTC := rustc
MKDIR := mkdir -p
BIN := bin/inject

all: $(BIN)

$(BIN): src/main.rs src/bmp.rs src/lsb_replacement.rs src/lsb_matching.rs
	$(MKDIR) bin
	$(RUSTC) $< -o $@

run: $(BIN)
	./bin/bmp rrnd ../content/001.bmp 30

clean:
	rm -f $(BIN)
